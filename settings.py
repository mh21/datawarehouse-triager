"""Settings file."""
import os

DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL')
DATAWAREHOUSE_TOKEN = os.environ.get('DATAWAREHOUSE_TOKEN')

EXCHANGE_NAME = os.environ.get('DATAWAREHOUSE_EXCHANGE_TRIAGER')
MESSAGE_QUEUE_NAME = os.environ.get('DATAWAREHOUSE_QUEUE_TRIAGER')

# Failure id from the Datawarehouse.
FAIL_KICKSTART = {'name': 'Kickstart Failure', 'id': 25}
NOT_FOUND = None
