"""Test traiger.py."""
import unittest
from unittest import mock

from triager import triager


class TestDWObject(unittest.TestCase):
    """Test DWObject."""

    def test_attrs(self):
        """Test getting attrs."""
        data = {'a': 1, 'b': 'two'}
        obj = triager.DWObject('foo', data)

        self.assertEqual(1, obj.a)
        self.assertEqual('two', obj.b)
        self.assertEqual(None, obj.c)


class TestTriager(unittest.TestCase):
    """Test Triager."""

    def test_dry_run(self):
        """Test dry_run flag."""
        self.assertTrue(triager.Triager(dry_run=True).dry_run)
        self.assertFalse(triager.Triager().dry_run)

    def test_check_revision(self):
        """Test check_revision."""
        obj = triager.Triager()
        self.assertEqual([], obj.check_revision(triager.DWObject('revision', {'id': 1})))

    def test_check_build(self):
        """Test check_build."""
        obj = triager.Triager()
        self.assertEqual([], obj.check_build(triager.DWObject('build', {'id': 1, 'valid': True})))

    def test_check_test_pass(self):
        """Test check_test."""
        obj = triager.Triager()
        test_pass = triager.DWObject(
            'test', {'id': 1, 'status': 'PASS'}
        )
        self.assertEqual([], obj.check_test(test_pass))

    @mock.patch('triager.triager.TestFailureChecker.check', mock.Mock(return_value=['foo', 'bar']))
    def test_check_test(self):
        """Test check_test."""
        obj = triager.Triager()
        test = triager.DWObject('test', {'id': 1, 'status': 'FAIL'})
        self.assertEqual(
            [
                (test, 'foo'),
                (test, 'bar'),
            ],
            obj.check_test(test)
        )

    @staticmethod
    def test_report_issue():
        """Test report issue."""
        obj = mock.Mock()
        issues = [
            (obj, {'id': 1}),
            (obj, {'id': 2}),
        ]

        triager.Triager().report_issues(issues)
        obj.issues.create.assert_has_calls(
            [
                mock.call(issue_id=1),
                mock.call(issue_id=2),
            ]
        )

    def test_report_issue_dry(self):
        """Test report issue. Dry run."""
        obj = mock.Mock()
        issues = [
            (obj, 1),
            (obj, 2),
        ]

        triager.Triager(dry_run=True).report_issues(issues)
        self.assertFalse(obj.issues.create.called)

    @mock.patch('triager.triager.Triager.check_test')
    def test_check_check_test(self, checker):
        """Test check."""
        triager_obj = triager.Triager()
        obj = triager.DWObject('test', {'id': 1})
        triager_obj.check('test', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)

    @mock.patch('triager.triager.Triager.check_revision')
    def test_check_check_revision(self, checker):
        """Test check."""
        triager_obj = triager.Triager()
        obj = triager.DWObject('revision', {'id': 1})
        triager_obj.check('revision', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)

    @mock.patch('triager.triager.Triager.check_build')
    def test_check_check_build(self, checker):
        """Test check."""
        triager_obj = triager.Triager()
        obj = triager.DWObject('build', {'id': 1})
        triager_obj.check('build', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)

    @mock.patch('triager.triager.Triager.check_build')
    def test_check_with_dict(self, checker):
        """Test check. With dict it creates a DWObject."""
        triager_obj = triager.Triager()
        obj = {'id': 1}
        triager_obj.check('build', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)

    @mock.patch('triager.triager.dw_client')
    @mock.patch('triager.triager.Triager.check_build')
    def test_check_with_id(self, checker, dw_client):
        """Test check. With id gets the obj from DW api."""
        triager_obj = triager.Triager()
        obj = '1'
        triager_obj.check('build', obj)
        self.assertTrue(checker.called)
        self.assertTrue(checker.report_issues)
        self.assertTrue(dw_client.kcidb.build.get)
